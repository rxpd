/*
    rx.c - rxpd plugin for xchat

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#define _GNU_SOURCE

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/uio.h>

#include "xchat-plugin.h"

#define PNAME "Rxpd"
#define PDESC "Uses rxpd to act on content";
#define PVERSION "0.1"

/*
  TODO
  * reconnect on connection loose
  * config-file and auto startup
*/


#define PREFIXCMP(str, pfx) (!strncmp (str, pfx, sizeof (pfx)-1))

static xchat_plugin *ph;   /* plugin handle */

static struct rx_plugin_data
{
  int debug;
  const char* server;
  const char* port;
  const char* prefix;
  const char* list;
  struct addrinfo* addrs;
  FILE* rxpd;
} rx_private;


void
xchat_plugin_get_info (char **name,
                       char **desc,
                       char **version,
                       void **reserved)
{
   *name = PNAME;
   *desc = PDESC;
   *version = PVERSION;
}

static FILE*
rxopen (struct rx_plugin_data* rx)
{
  if (!rx->addrs)
    {
      xchat_printf (ph, "RX: no server set up");
      return NULL;
    }

  int fd;

  fd = socket (rx->addrs->ai_family, rx->addrs->ai_socktype, rx->addrs->ai_protocol);
  if (fd == -1 || connect (fd, rx->addrs->ai_addr, rx->addrs->ai_addrlen))
    {
      xchat_printf (ph, "RX: error connecting %s:%s, %s\n", rx->server, rx->port, strerror (errno));
      return NULL;
    }

  FILE* conn;
  conn = fdopen (fd, "rb+");
  if (!conn)
    {
      close (fd);
      xchat_printf (ph, "RX: error connecting %s:%s, %s\n", rx->server, rx->port, strerror (errno));
      return NULL;
    }

  return conn;
}


static int
rxstart_cb (char *word[], char *word_eol[], void *userdata)
{
  struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;

  if (rx->rxpd)
    {
      xchat_printf (ph, "RX: already connected");
      return XCHAT_EAT_ALL;
    }

  rx->server = strdup (word[2]);
  rx->port = strdup (word[3]);
  rx->list = strdup (word[4]);
  rx->prefix = strdup (word[5]);

  int aierr;

  /* resolve peer */
  aierr = getaddrinfo (rx->server, rx->port, NULL, &rx->addrs);
  if (aierr)
    {
      xchat_printf (ph, "could not resolve %s:%s, %s\n", rx->server, rx->port, gai_strerror (aierr));
      return XCHAT_EAT_ALL;
    }


  rx->rxpd = rxopen (rx);
  if (!rx->rxpd)
    {
      freeaddrinfo (rx->addrs);
      rx->addrs = NULL;
      return XCHAT_EAT_ALL;
    }

  fprintf (rx->rxpd, "CHECK:%s%s\n", rx->prefix, rx->list);
  /*error handling here*/

  xchat_printf (ph, "established rxpd connection to %s:%s\n", rx->server, rx->port);

  return XCHAT_EAT_ALL;
}

static int
rxstop_cb (char *word[], char *word_eol[], void *userdata)
{
  struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;

  if (rx->rxpd)
    {
      fclose (rx->rxpd);
      rx->rxpd = NULL;
      freeaddrinfo (rx->addrs);
      rx->addrs = NULL;
      xchat_printf (ph, "closed rxpd connection to %s:%s\n", rx->server, rx->port);
    }
  else
    xchat_printf (ph, "no rxpd connection established\n");

  return XCHAT_EAT_ALL;
}

static int
rxadd_cb (char *word[], char *word_eol[], void *userdata)
{
  struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;

  if (*word[2] == '\0' || *word[3] == '\0')
    {
      xchat_printf (ph, "RXADD needs two parameters (/RXADD list rule...)\n");
      return XCHAT_EAT_ALL;
    }

  FILE* conn;
  conn = rxopen (rx);
  if (!conn)
    return XCHAT_EAT_ALL;

  if (fprintf (conn, "APPEND:%s%s\n%s\n!EXIT\n", rx->prefix, word[2], word_eol[3]) < 0)
    {
      xchat_printf (ph, "RX: sending command failed\n");
      fclose (conn);
      return XCHAT_EAT_ALL;
    }

  char buffer[4096];
  *buffer = '\0';
  fgets (buffer, 4095, conn);
  xchat_printf (ph, "RX: add '%s' %s", word_eol[3], buffer);

  fclose (conn);

  conn = rxopen (rx);
  if (!conn)
    return XCHAT_EAT_ALL;

  if (fprintf (conn, "SAVE:%s%s\n", rx->prefix, word[2]) < 0)
    xchat_printf (ph, "RX: saving failed\n");
  fclose (conn);
  return XCHAT_EAT_ALL;
}

static int
rxdel_cb (char *word[], char *word_eol[], void *userdata)
{
  struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;

  if (*word[2] == '\0' || *word[3] == '\0')
    {
      xchat_printf (ph, "RXDEL needs two parameters (/RXADD list rule...)\n");
      return XCHAT_EAT_ALL;
    }

  FILE* conn;
  conn = rxopen (rx);
  if (!conn)
    return XCHAT_EAT_ALL;

  if (fprintf (conn, "REMOVE:%s%s\n%s\n!EXIT\n", rx->prefix, word[2], word_eol[3]) < 0)
    {
      xchat_printf (ph, "RX: sending command failed\n");
      fclose (conn);
      return XCHAT_EAT_ALL;
    }

  char buffer[4096];
  *buffer = '\0';
  fgets (buffer, 4095, conn);
  xchat_printf (ph, "RX: del '%s' %s", word_eol[3], buffer);

  fclose (conn);
  conn = rxopen (rx);
  if (!conn)
    return XCHAT_EAT_ALL;

  if (fprintf (conn, "SAVE:%s%s\n", rx->prefix, word[2]) < 0)
    xchat_printf (ph, "RX: saving failed\n");
  fclose (conn);
  return XCHAT_EAT_ALL;
}

static int
rxlist_cb (char *word[], char *word_eol[], void *userdata)
{
  struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;

  FILE* conn = rxopen (rx);
  if (!conn)
    return XCHAT_EAT_ALL;

  const char* list = *word[2]?word[2]:rx->list;

  xchat_printf (ph, "\nRX: Listing '%s%s'\n", rx->prefix, list);

  fprintf (conn, "DUMPH:%s%s\n", rx->prefix, list);
  /*error handling here*/

  char buffer[4096];
  while (fgets (buffer, 4095, conn))
    {
      xchat_printf (ph, "%s", buffer);
    }
  xchat_printf (ph, "\n");

  fclose (conn);
  return XCHAT_EAT_ALL;
}

#if 0
static int
rxraw_cb (char *word[], char *word_eol[], void *userdata)
{
  //struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;
  return XCHAT_EAT_ALL;   /* eat this command so xchat and other plugins can't process it */
}
#endif

static int
rxdebug_cb (char *word[], char *word_eol[], void *userdata)
{
  struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;
  rx->debug ^= 1;
  xchat_printf (ph, "Turned rxpd debugging %s\n", rx->debug?"on":"off");  
  return XCHAT_EAT_ALL;
}

static int
rxinfo_cb (char *word[], char *word_eol[], void *userdata)
{
  struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;
  xchat_printf (ph, "RX: debugging is %s\n", rx->debug?"on":"off");  
  if (rx->addrs)
    {
      xchat_printf (ph, "RX: using server %s:%s\n", rx->server, rx->port);  
      xchat_printf (ph, "RX: using prefix %s\n", rx->prefix);
      xchat_printf (ph, "RX: using list   %s%s\n", rx->prefix, rx->list);
      xchat_printf (ph, "RX: connection is %s\n", rx->rxpd?"active":"broken");  
    }
  else
    xchat_printf (ph, "RX: not started\n");

  return XCHAT_EAT_ALL;
}

static int
rxhook_cb (char *word[], char *word_eol[], void *userdata)
{
  struct rx_plugin_data* rx = (struct rx_plugin_data*)userdata;

  if (rx->rxpd)
    {
      /* ok do the checking*/
      if (fprintf (rx->rxpd, "%s\n", word_eol[1]) < 0)
        {
          xchat_printf (ph, "RX: rxpd connection lost\n");
          fclose (rx->rxpd);
          rx->rxpd = NULL;
          return XCHAT_EAT_NONE;
        }

      char buffer[4096];

      if (!fgets (buffer, 4095, rx->rxpd))
        {
          xchat_printf (ph, "RX: rxpd connection lost\n");
          fclose (rx->rxpd);
          rx->rxpd = NULL;
          return XCHAT_EAT_NONE;
        }

      char* nl = strrchr(buffer, '\n');
      if (nl)
        *nl = '\0';

      if (rx->debug && !PREFIXCMP(buffer, "ok:"))
        xchat_printf (ph, "RX: '%s' matched '%s'\n", word_eol[1], buffer);

      if (PREFIXCMP(buffer, "ignore:"))
        return XCHAT_EAT_XCHAT;

      else if (PREFIXCMP(buffer, "op:"))
        {
          const char* iam = xchat_get_info (ph, "nick");

          char* hisnickend = strchr (word_eol[1]+1, '!');
          if (hisnickend)
            {
              char* hisnick = strndupa (word_eol[1]+1, hisnickend - word_eol[1]-1);

              int op_him = 0;

              xchat_list* users = xchat_list_get (ph, "users");
              if (users)
                {
                  while (xchat_list_next (ph, users))
                    {
                      if (!strcmp (iam, xchat_list_str (ph, users, "nick")))
                        {
                          if (*xchat_list_str (ph, users, "prefix") != '@')
                            {
                              op_him = 0;
                              break;
                            }
                        }

                      if (!strcmp (hisnick, xchat_list_str (ph, users, "nick")))
                        {
                          if (*xchat_list_str (ph, users, "prefix") != '@')
                            op_him = 1;
                          else
                            {
                              op_him = 0;
                              break;
                            }
                        }
                    }
                  xchat_list_free (ph, users);

                  if (op_him)
                    xchat_commandf (ph, "OP %s\n", hisnick);
                }
            }
        }
      //else if (PREFIXCMP(buffer, "kick:"))
      //else if (PREFIXCMP(buffer, "kickban:"))
      //else if (PREFIXCMP(buffer, "ban:"))
    }

  return XCHAT_EAT_NONE;
}

static struct commands
{
  const char* command;
  const char* help;
  int (*callback) (char *[], char *[], void *);
} command_table[] =
  {
    {"RXSTART", "Usage: RXSTART server port list prefix; enables and connects to rxpd", rxstart_cb},
    {"RXSTOP", "Usage: RXSTOP; disconnects and stops rxpd plugin", rxstop_cb},
    {"RXADD", "Usage: RXADD listname rule; adds rule to list", rxadd_cb},
    {"RXDEL", "Usage: RXDEL list rule; removes rule from list", rxdel_cb},
    {"RXLIST", "Usage: RXLIST [list]; shows list", rxlist_cb},
    //{"RXRAW", "Usage: RXRAW ...; sends a raw command to the rxpd", rxraw_cb},
    {"RXDEBUG", "Usage: RXDEBUG; toggle rxpd plugin debugging", rxdebug_cb},
    {"RXINFO", "Usage: RXINFO; show some information about the rxpd plugin", rxinfo_cb},
    {NULL, NULL}
  };


int
xchat_plugin_init (xchat_plugin *plugin_handle,
                   char **plugin_name,
                   char **plugin_desc,
                   char **plugin_version,
                   char *arg)
{
  /* we need to save this for use with any xchat_* functions */
  ph = plugin_handle;

  /* tell xchat our info */
  *plugin_name = PNAME;
  *plugin_desc = PDESC;
  *plugin_version = PVERSION;

  rx_private.debug = 0;
  rx_private.server = NULL;
  rx_private.port = NULL;
  rx_private.prefix = NULL;
  rx_private.list = NULL;
  rx_private.addrs = NULL;
  rx_private.rxpd = NULL;

  struct commands* itr;
  for (itr = command_table; itr->command; ++itr)
    xchat_hook_command (ph, itr->command, XCHAT_PRI_NORM, itr->callback,
                        itr->help, &rx_private);

  char buffer[4096];
  *buffer = '\0';
  snprintf (buffer, 4095, "%s/rxpd.conf", xchat_get_info(ph, "xchatdirfs"));
  FILE* conf = fopen (buffer, "r");
  if (conf)
    {
      if (fgets (buffer, 4095, conf))
        {
          char* nl = strchr (buffer, '\n');
          if (nl)
            *nl = '\0';
          xchat_commandf (ph, "RXSTART %s", buffer);
        }
      fclose (conf);
    }

  xchat_hook_server (ph, "RAW LINE", XCHAT_PRI_NORM, rxhook_cb, &rx_private);

  xchat_print (ph, "Rxpd plugin loaded successfully!\n");

  return 1;
}

int
xchat_plugin_deinit (void)
{
  if (rx_private.rxpd)
    fclose (rx_private.rxpd);

  if (rx_private.addrs)
    freeaddrinfo (rx_private.addrs);

  xchat_printf (ph, "rxpd plugin unloaded\n");

  return 1;
} 
