/*
    main.c - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rxpd.h"

#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <locale.h>
#include <langinfo.h>

void
version (void)
{
  printf (
          "  Regex Policy Daemon ("PACKAGE_STRING")\n\n"
          "  Copyright (C)\n"
          "    2007,               Christian Thaeter <ct@pipapo.org>\n\n"
          "  This is free software.  You may redistribute copies of it under the terms of\n"
          "  the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.\n"
          "  There is NO WARRANTY, to the extent permitted by law.\n\n"
          "  http://www.pipapo.org/pipawiki/RegexPolicyDaemon\n"
          );
}

void
usage (void)
{
  printf (
    "rxpd [OPTIONS] RULES..\n"
    " -v          increase verbosity level\n"
    " -V          show version\n"
    " -d          daemonize into background\n"
    " -D          debug mode\n"
    " -b dir      basedir for rules\n"
    " -q          be quiet\n"
    " -t port     listen on tcp port\n"
    //" -u name     unix\n"
    " -p policy   define a list for access policies\n"
    " -i          case insensitive regex\n"
    //" -4          ipv4\n"
    //" -6          ipv6\n"
    //" -r          resolve names\n"
    //" -l log      log hits to logfile\n"
    " -L locale   set locale, must be a utf-8 locale, only LC_CTYPE is used\n"
    " -U user     if started as root, run as user [default: nobody]\n"
    " -h          this usage information\n"
    );
}


int
main (int argc, char** argv)
{
  if (pth_init() == FALSE)
    rxpd_fail (NULL, "pth initialization failed\n");

  struct rxpd_base* rxpd = rxpd_init ();

  sigset_t sigs;
  sigemptyset (&sigs);
  sigaddset (&sigs, SIGHUP);
  sigaddset (&sigs, SIGTERM);
  sigaddset (&sigs, SIGINT);
  sigaddset (&sigs, SIGALRM);

  pth_sigmask (SIG_BLOCK, &sigs, NULL);

  openlog (PACKAGE_NAME, LOG_PID, LOG_DAEMON);

  opterr = 0;
  int opt;

  while ((opt = getopt (argc, argv, "vVdDb:qt:u:p:i46rl:L:U:h")) != -1)
    switch (opt)
      {
      case 'v':
        if (rxpd->verbosity < LOG_DEBUG)
          ++rxpd->verbosity;
        break;
      case 'V':
        version ();
        exit (EXIT_SUCCESS);
      case 'd':
        rxpd->daemonize = 1;
        break;
      case 'D':
        if (rxpd->verbosity < LOG_INFO)
          rxpd->verbosity = LOG_INFO;
        rxpd->daemonize = 0;
        break;
      case 'b':
        if (!rxpd->basedir)
          rxpd->basedir = rxpd_strdup (optarg);
        else
          rxpd_fail (rxpd, "basedir already set\n");
        break;
      case 'q':
        rxpd->verbosity = LOG_ALERT;
        break;
      case 't':
        {
          int port = atoi (optarg);
          if (port > 0 && port < 65536)
            {
              if (!rxpd_socket_new_tcp4 (rxpd, NULL, port))
                rxpd_fail (rxpd, "Could not open listening socket on port %d\n", port);
            }
          else
            rxpd_fail (rxpd, "Illegal port number\n");
        }
        // TODO rxpd_socket_new_tcp6 (rxpd, NULL, 2374)
        break;
#if 0 /*not yet implemented*/
      case 'u':
        //rxpd_socket_new_unix (rxpd, NULL, 2374);
        break;
#endif
      case 'p':
        if (!rxpd->policy)
          rxpd->policy = rxpd_file_new (rxpd, optarg);
        else
          rxpd_fail (rxpd, "policy already set\n");
        break;
      case 'i':
        rxpd->regflags |= REG_ICASE;
        break;
      case 'L':
        if (!rxpd->locale)
          rxpd->locale = optarg;
        else
          rxpd_fail (rxpd, "locale already set\n");
        break;
#if 0 /*not yet implemented*/
      case '4':
        break;
      case '6':
        break;
      case 'r':
        break;
      case 'l:':
        break;
#endif
      case 'U':
        if (!rxpd->user)
          rxpd->user = rxpd_strdup (optarg);
        else
          rxpd_fail (rxpd, "user already set\n");
        break;
      case 'h':
        usage ();
        exit (0);
        break;
      default:
        rxpd_fail (rxpd, "Unknown option '-%c'\n", opt);
      }

  if (!rxpd->basedir)
    rxpd_fail (rxpd, "Basedir not set (use -b BASEDIR)\n");

  if (llist_is_empty (&rxpd->sockets))
    rxpd_fail (rxpd, "No listening sockets given (use -t TCP or -u UNIX)\n");

  rxpd->locale = setlocale(LC_CTYPE, rxpd->locale ? rxpd->locale : "");
  if (!rxpd->locale)
    rxpd_fail (rxpd, "Failed setting locale\n");

  rxpd_log (rxpd, LOG_INFO, "Using locale '%s'\n", rxpd->locale);
  if (strcmp(nl_langinfo(CODESET), "UTF-8") != 0)
    rxpd_fail (rxpd, "Not a utf-8 locale '%s'\n", rxpd->locale);

  rxpd_log (rxpd, LOG_NOTICE, PACKAGE_STRING" starting up\n");

  rxpd_enter_personality (rxpd);

  if (rxpd->policy)
    {
      if (rxpd_file_load (rxpd->policy))
        rxpd_log (rxpd, LOG_INFO, "Loaded policy '%s'\n", rxpd->policy->node.key);
      else
        rxpd_fail (rxpd, "Failed loading policy '%s'\n", rxpd->policy->node.key);
    }

  for (int i = optind; i < argc; ++i)
    {
      if (!rxpd_file_load (rxpd_file_new (rxpd, argv[i])))
        rxpd_fail (rxpd, "Failed loading file '%s'\n", argv[i]);
    }

  LLIST_FOREACH (&rxpd->sockets, n)
    {
      struct rxpd_socket* socket = (struct rxpd_socket*)n;
      rxpd_socket_spawn (socket);
    }

  rxpd_log (rxpd, LOG_NOTICE, PACKAGE_STRING" running\n");

  /* the main thread sits wnd waits for signals */
  rxpd_signalloop (rxpd);

  LLIST_WHILE_HEAD (&rxpd->sockets, n)
    {
      struct rxpd_socket* socket = (struct rxpd_socket*)n;
      rxpd_socket_delete (rxpd_socket_join (socket));
    }

  rxpd_log (rxpd, LOG_NOTICE, PACKAGE_STRING" exited\n");
  rxpd_destroy ();
  pth_kill ();

  return EXIT_SUCCESS;
}
