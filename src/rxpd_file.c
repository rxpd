/*
    rxpd_file.c - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rxpd.h"

struct rxpd_file*
rxpd_file_new (struct rxpd_base* base, const char* filename)
{
  struct rxpd_file* self = NULL;

  while (*filename == '/')
    ++filename;

  if (!filename ||
      strcspn (filename, RXPD_FILE_ILG_CHARS) != strlen (filename) ||
      strstr (filename, "../") ||
      strstr (filename, "/./") ||
      filename[strlen (filename) - 1] == '/' ||
      strlen (filename) + strlen (base->basedir) > 4095)
    {
      rxpd_log (base, LOG_ERR, "illegal filename: '%s'\n", filename?filename:"");
      return NULL;
    }

  self = rxpd_malloc (sizeof (struct rxpd_file));
  self->base = base;
  pth_rwlock_init (&self->lock);

  psplay_init (&self->node, rxpd_strdup (filename));
  llist_init (&self->rules);

  psplay_insert (&base->files, &self->node);

  rxpd_log (base, LOG_INFO, "new file: '%s'\n", filename);
  return self;
}

void
rxpd_file_delete (struct rxpd_file* self)
{
  if (self)
    {
      /* psplay remove does not preempt, no locking needed */
      /* pth_rwlock_acquire (&self->lock, PTH_RWLOCK_RW, FALSE, NULL); */
      psplay_remove (&self->base->files, &self->node);
      /* pth_rwlock_release (&self->lock); */

      rxpd_file_rules_delete (self);

      free ((void*)self->node.key);
      free (self);
    }
}

void
rxpd_file_remove (struct rxpd_file* self)
{
  if (self)
    {
      /* psplay remove does not preempt, no locking needed */
      /* pth_rwlock_acquire (&self->lock, PTH_RWLOCK_RW, FALSE, NULL); */
      psplay_remove (&self->base->files, &self->node);
      /* pth_rwlock_release (&self->lock); */

      rxpd_file_rules_delete (self);

      while (1)
        {
          rxpd_log (self->base, LOG_INFO, "delete file: '%s'\n", (char*)self->node.key);
          remove ((char*)self->node.key);
          char* slash = strrchr ((char*)self->node.key, '/');
          if (slash)
            *slash = '\0';
          else
            break;
        }

      free ((void*)self->node.key);
      free (self);
    }
}

struct rxpd_file*
rxpd_file_rules_delete (struct rxpd_file* self)
{
  if (self)
    {
      pth_rwlock_acquire (&self->lock, PTH_RWLOCK_RW, FALSE, NULL);
      LLIST_WHILE_HEAD (&self->rules, n)
        {
          struct rxpd_rule* node = (struct rxpd_rule*)n;
          rxpd_rule_delete (node);
        }
      pth_rwlock_release (&self->lock);
    }
  return self;
}

int
rxpd_file_load (struct rxpd_file* self)
{
  FILE* f = fopen (self->node.key, "r");
  if (f)
    {
      pth_rwlock_acquire (&self->lock, PTH_RWLOCK_RW, FALSE, NULL);

      /* First purge old rules */
      LLIST_WHILE_HEAD (&self->rules, n)
        {
          struct rxpd_rule* node = (struct rxpd_rule*)n;
          rxpd_rule_delete (node);
        }

      // TODO test excess line length = error
      char buf[4096];

      rxpd_log (self->base, LOG_NOTICE, "loading '%s'\n", self->node.key);

      while (fgets (buf, 4096, f))
        {
          size_t last = strlen(buf);
          if (buf[last-1] == '\n')
            buf[last-1] = '\0';

          struct rxpd_rule* rule;
          rule = rxpd_rule_new (buf, self->base);
          rxpd_log (self->base, LOG_DEBUG, "new rule '%s'\n", rule->string);

          llist_insert_tail (&self->rules, &rule->node);
        }

      pth_rwlock_release (&self->lock);
      fclose (f);
      return 1;
    }
  else
    {
      rxpd_log (self->base, LOG_ERR, "failed loading '%s'\n", self->node.key);
      return 0;
    }
}


int
rxpd_file_save (struct rxpd_file* self, int force)
{
  char* filename = strdupa (self->node.key);

  if (!force && access (filename, F_OK))
    {
      rxpd_log (self->base, LOG_NOTICE, "not saving non existent file '%s'\n", filename);
      return 0;
    }

  char* slash = filename;

  while ((slash = strchr (slash+1, '/')))
    {
      *slash = '\0';
      mkdir (filename, 0777);
      *slash = '/';
    }

  FILE* f = fopen (filename, "w");
  if (f)
    {
      pth_rwlock_acquire (&self->lock, PTH_RWLOCK_RD, FALSE, NULL);

      LLIST_FOREACH (&self->rules, n)
        {
          struct rxpd_rule* node = (struct rxpd_rule*)n;
          if (node->atime != (time_t)-1)
            fprintf (f, "%ld:%s\n", node->atime, node->string);
          else if (*node->string != '#')
            fprintf (f, ":%s\n", node->string);
          else
            fprintf (f, "%s\n", node->string);
        }

      pth_rwlock_release (&self->lock);
      fclose (f);
      rxpd_log (self->base, LOG_NOTICE, "saved '%s'\n", filename);
      return 1;
    }
  else
    {
      rxpd_log (self->base, LOG_ERR, "failed saving '%s'\n", filename);
      return 0;
    }
}

struct rxpd_file*
rxpd_file_dump (struct rxpd_file* self, struct rxpd_buffer* out, const char* fmt)
{
  if (self)
    {
      if (llist_is_empty (&self->rules))
        rxpd_buffer_printf (out, "#OK:\n");
      else
        {
          pth_rwlock_acquire (&self->lock, PTH_RWLOCK_RD, FALSE, NULL);

          LLIST_FOREACH (&self->rules, n)
            {
              struct rxpd_rule* rule = (struct rxpd_rule*)n;
              if (rule->atime != (time_t)-1)
                {
                  if (fmt)
                    {
                      char buf[64];
                      struct tm *time;

                      time = gmtime(&rule->atime);
                      strftime(buf, sizeof(buf)-1, fmt, time);
                      rxpd_buffer_printf (out, "%s:%s\n", buf, rule->string);
                    }
                  else
                    rxpd_buffer_printf (out, "%ld:%s\n", rule->atime, rule->string);
                }
              else if (*rule->string != '#')
                rxpd_buffer_printf (out, ":%s\n", rule->string);
              else
                rxpd_buffer_printf (out, "%s\n", rule->string);
            }
          pth_rwlock_release (&self->lock);
        }
    }

  return self;
}

int
rxpd_file_cmp (const void* A, const void* B)
{
  return strcmp (A, B);
}


