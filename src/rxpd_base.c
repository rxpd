/*
    rxpd_base.c - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rxpd.h"

static struct rxpd_base global_base;

struct rxpd_base*
rxpd_init (void)
{
  if (global_base.basedir)
    return NULL;

  global_base.basedir = NULL;

  global_base.verbosity = LOG_WARNING;
  global_base.daemonize = 0;
  global_base.regflags = 0;
  global_base.locale = NULL;
  global_base.user = NULL;
  global_base.policy = NULL;
  global_base.main = pth_self ();
  pth_mutex_init (&global_base.nest_lock);

  psplay_init_root (&global_base.files, rxpd_file_cmp, (psplay_delete_t)rxpd_file_delete);
  llist_init (&global_base.sockets);

  rxpd_log (&global_base, LOG_DEBUG, PACKAGE_NAME" initialized\n");
  return &global_base;
}


void
rxpd_destroy (void)
{
  if (global_base.basedir)
    {
      free (global_base.basedir);
      psplay_destroy_root (&global_base.files);
      LLIST_WHILE_HEAD (&global_base.sockets, n)
        {
          struct rxpd_socket* socket = (struct rxpd_socket*)n;
          rxpd_socket_delete (socket);
        }
    }
}

void
rxpd_log (struct rxpd_base* self, int level, const char* fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  if (level <= (self?self->verbosity:LOG_DEBUG))
    {
      if (!self || self->daemonize)
        {
          va_list as;
          va_copy (as, ap);
          vsyslog (level, fmt, as);
          va_end (as);
        }
      vfprintf (stderr, fmt, ap);
    }
  va_end (ap);
}

void
rxpd_fail (struct rxpd_base* self, const char* fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  if (!self || self->daemonize)
    {
      va_list as;
      va_copy (as, ap);
      vsyslog (LOG_ALERT, fmt, as);
      va_end (as);
    }
  vfprintf (stderr, fmt, ap);
  va_end (ap);
  exit (EXIT_FAILURE);
}

void
rxpd_die (const char* fmt, ...)
{
  va_list ap, as;
  va_start(ap, fmt);
  va_copy (as, ap);
  vsyslog (LOG_EMERG, fmt, ap);
  vfprintf (stderr, fmt, ap);
  va_end (as);
  va_end (ap);
  abort ();
}

void*
rxpd_malloc (size_t size)
{
  void* r;
  r = malloc (size);
  if (!r)
    rxpd_die ("Out of Memory\n");
  return r;
}

char*
rxpd_strdup (const char* str)
{
  char* r;
  r = strdup (str);
  if (!r)
    rxpd_die ("Out of Memory\n");
  return r;
}

void
rxpd_enter_personality (struct rxpd_base* self)
{
  umask (0022);

  if (chdir (self->basedir))
    rxpd_fail (self, "couldn't change to basedir '%s'\n", self->basedir);

  if (getuid() == 0  ||  geteuid() == 0)
    {
      struct passwd *pw;

      if (!self->user)
        self->user = rxpd_strdup ("nobody");

      pw = getpwnam (self->user);
      if (!pw)
        rxpd_fail (self, "couldn't find user '%s'\n", self->user);

      if (setgid (pw->pw_gid))
        rxpd_fail (self, "setgid failed\n");

      if (setgroups (1, &pw->pw_gid))
        rxpd_fail (self, "setgroups failed\n");

      if (setuid (pw->pw_uid))
        rxpd_fail (self, "setuid failed\n");
    }

  if (self->daemonize)
    {
      int n = open("/dev/null",O_RDWR);
      if (n < 0)
        rxpd_fail (self, "Couldn't open /dev/null\n");
      dup2(n, 0);
      dup2(n, 1);
      dup2(n, 2);
      close (n);

      pid_t pid = fork();
      if (pid < 0)
        rxpd_fail (self, "Couldn't daemonize\n");
      if (pid > 0)
        exit (0);

      setsid();
    }
}

static psplay_delete_t
loadsave_files (PSplay node, const enum psplay_order_e which, int level, void* data)
{
  (void) level;
  struct rxpd_file* file = (struct rxpd_file*) node;

  if (which == PSPLAY_INORDER)
    {
      if (data)
        rxpd_file_load (file);
      else
        rxpd_file_save (file, 0);
    }

  return PSPLAY_CONT;
}


void
rxpd_signalloop (struct rxpd_base* base)
{
  sigset_t sigs;
  sigemptyset (&sigs);
  sigaddset (&sigs, SIGHUP);
  sigaddset (&sigs, SIGTERM);
  sigaddset (&sigs, SIGINT);
  sigaddset (&sigs, SIGALRM);

  int signal;

  while (!pth_sigwait (&sigs, &signal))
    {
      switch (signal)
        {
        case SIGHUP:
          /* reload files from disk */
          rxpd_log (base, LOG_NOTICE, "reloading all files\n");
          if (!psplay_isempty_root (&base->files))
            psplay_walk (&base->files, NULL, loadsave_files, 0, base /*just some non-NULL value*/);
          break;
        case SIGTERM:
          /* save files and exit */
          rxpd_log (base, LOG_NOTICE, "saving all files, terminating\n");
          if (!psplay_isempty_root (&base->files))
            psplay_walk (&base->files, NULL, loadsave_files, 0, NULL);

          /* fallthrough */
        case SIGINT:
          /* exit without saving */
          rxpd_log (base, LOG_NOTICE, "terminating\n");
          LLIST_FOREACH (&base->sockets, n)
            {
              struct rxpd_socket* socket = (struct rxpd_socket*)n;
              rxpd_socket_cancel (socket);
            }
          return;
        case SIGALRM:
          /* ALRM: save files */
          rxpd_log (base, LOG_NOTICE, "saving all files\n");
          if (!psplay_isempty_root (&base->files))
            psplay_walk (&base->files, NULL, loadsave_files, 0, NULL);

          break;
        }
    }
}
