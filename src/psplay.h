/*
    psplay.h - probabilistic splay tree

  Copyright (C)
    2004, 2005, 2006,   Christian Thaeter <chth@gmx.net>
  Copyright (C)         CinelerraCV
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef PSPLAY_H
#define PSPLAY_H

#include <stdint.h>
#include <stdio.h>

enum psplay_order_e
  {
    PSPLAY_PREORDER,
    PSPLAY_INORDER,
    PSPLAY_POSTORDER
  };

typedef struct psplayroot_struct psplayroot;
typedef psplayroot* PSplayroot;

typedef struct psplay_struct psplay;
typedef psplay* PSplay;

typedef int (*psplay_cmp_t)(const void*, const void*);
typedef void (*psplay_delete_t)(PSplay);
typedef psplay_delete_t (*psplay_action_t)(PSplay node, const enum psplay_order_e which, int level, void* data);

//
struct psplayroot_struct
{
  PSplay tree;
  psplay_cmp_t cmp;
  psplay_delete_t delete;
  /* amount of elements will some day adjust the probability fucntions, further research necessary */
  size_t elem_cnt;
  /* PSplay history[64]; ringbuffer storing the path of a search, optimize 'up' pointer out */
};

PSplayroot
psplay_init_root (PSplayroot self, psplay_cmp_t cmp, psplay_delete_t delete);

PSplayroot
psplay_destroy_root (PSplayroot self);

static inline int
psplay_isempty_root (PSplayroot root)
{
  return !root->tree;
}

#define PSPLAYROOT_INITIALIZER(cmp, delete) {NULL, cmp, delete, 0}

//
struct psplay_struct
{
  const void* key;
  PSplay up;
  PSplay left;
  PSplay right;
};


PSplay
psplay_new (void * key);

PSplay
psplay_init (PSplay self, const void* key);

void
psplay_delete (PSplay node);

PSplay
psplay_insert (PSplayroot root, PSplay node);

PSplay
psplay_find (PSplayroot root, void* key);

PSplay
psplay_remove (PSplayroot root, PSplay node);

extern const psplay_delete_t PSPLAY_CONT;
extern const psplay_delete_t PSPLAY_STOP;
extern const psplay_delete_t PSPLAY_REMOVE;

int
psplay_walk (PSplayroot root, PSplay node, psplay_action_t action, int level, void* data);


void
psplay_dump (PSplayroot root, FILE* dest);


#endif
