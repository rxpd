/*
    rxpd_client.h - regex policy daemon, client library

  Copyright (C)
    2007, 2008,         Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

struct rxpd_client
{
  char* address;
  char* port;
  struct addrinfo* ai;

  char* prefix;
  FILE* handle;
  char buf[4096];
};
typedef struct rxpd_client* RxpdClient;

RxpdClient
rxpd_client_init (RxpdClient self, const char* address, const char* prefix);

RxpdClient
rxpd_client_destroy (RxpdClient self);

void
rxpd_client_free (RxpdClient self);

RxpdClient
rxpd_client_new (const char* address, const char* prefix);

int
rxpd_client_open (RxpdClient self);

void
rxpd_client_close (RxpdClient self);

int
rxpd_client_cmd (RxpdClient self, const char* command, const char* list);

int
rxpd_client_query (RxpdClient self, const char* fmt, ...);

int
rxpd_client_pending (RxpdClient self, int timeout);

const char*
rxpd_client_recieve (RxpdClient self);
