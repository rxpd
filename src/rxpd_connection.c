/*
    rxpd_connection.c - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rxpd.h"

struct rxpd_connection*
rxpd_connection_new (struct rxpd_socket* socket, int fd)
{
  struct rxpd_connection* self;
  self = rxpd_malloc (sizeof (struct rxpd_connection));

  llist_init (&self->node);
  self->fd = fd;
  self->socket = socket;
  self->file = NULL;
  self->tmp_str = NULL;
  llist_init (&self->tmp_list);

  rxpd_buffer_init (&self->in, fd);
  rxpd_buffer_init (&self->out, fd);

  self->connecter = NULL;
  llist_insert_tail (&socket->connections, &self->node);

  return self;
}

void
rxpd_connection_delete (struct rxpd_connection* self)
{
  if (self)
    {
      if (self->fd != -1)
        close (self->fd);
      free (self->tmp_str);
      LLIST_WHILE_HEAD (&self->tmp_list, n)
        {
          struct rxpd_rule* node = (struct rxpd_rule*)n;
          rxpd_rule_delete (node);
        }
    }
  free (self);
}

static void
rxpd_connection_cleanup (void* ptr)
{
  struct rxpd_connection* self = ptr;
  llist_unlink (&self->node);

  char buf[512];
  *buf = '\0';
  if (self->socket->rxpd_socket_addr (self, buf, "", 511))
    {
      rxpd_log (NULL, LOG_DEBUG, "connection %s terminated\n", buf);
    }

  close (self->fd);
  self->fd = -1;
  rxpd_connection_delete (self);
}


struct rxpd_connection*
rxpd_connection_spawn (struct rxpd_connection* self)
{
  if (self)
    {
      if (self->connecter)
        rxpd_die ("connection thread already spawned\n");

      pth_attr_t attr = pth_attr_new ();

      pth_attr_set (attr, PTH_ATTR_JOINABLE, FALSE);

      self->connecter = pth_spawn (attr, rxpd_connection_parse_cmd, self);

      if (!self->connecter)
        rxpd_die ("failed spawning thread\n");
    }
  return self;
}

int
rxpd_connection_check_policy (struct rxpd_connection* self, char* line)
{
  struct rxpd_base* base = self->socket->base;
  if (base->policy)
    {
      char buf[256];
      buf[0] = '\0';

      if (!self->socket->rxpd_socket_addr (self, buf, line, 256))
        {
          rxpd_log (base, LOG_ERR, "policy line too long\n");
          return 0;
        }

      char* match = NULL;
      LLIST_FOREACH (&base->policy->rules, n)
      {
        struct rxpd_rule* rule = (struct rxpd_rule*)n;
        if (rule->string[0] != '#')
          {
            if (regexec (&rule->rx, buf, 0, NULL, 0) == 0)
              {
                match = rule->string;
                break;
              }
          }
      }

      if (!match || !RXPD_PREFIXCMP (match, "ACCEPT:"))
        {
          rxpd_log (base, LOG_WARNING, "policy check: access denied '%s'\n", buf);
          return 0;
        }
      rxpd_log (base, LOG_NOTICE, "policy check: permitted '%s'\n", buf);
    }
  return 1;
}

struct rxpd_connection*
rxpd_connection_cancel (struct rxpd_connection* self)
{
  llist_unlink (&self->node);
  pth_cancel (self->connecter);
  return self;
}

void*
rxpd_connection_parse_cmd (void* ptr)
{
  struct rxpd_connection* self = (struct rxpd_connection*) ptr;
  struct rxpd_base* base = self->socket->base;

  pth_cleanup_push (rxpd_connection_cleanup, ptr);

  char* line;
  line = rxpd_buffer_readline (&self->in);

  if (!line)
    {
      rxpd_log (base, LOG_ERR, "no data\n");
      rxpd_buffer_printf (&self->out, "#ERROR: no data\n");
      goto end;
    }

  rxpd_log (base, LOG_DEBUG, "parse command '%s'\n", line);

  static const struct cmd_table
  {
    enum rxpd_cmd_e nr;
    const char* cmd;
    size_t sz;
  } cmds[] =
    {
#define RXPD_CMD(cmd, _) {RXPD_CMD_##cmd, #cmd":", sizeof (#cmd)},
      RXPD_COMMANDS
#undef RXPD_CMD
      {0, NULL, 0}
    };

  const struct cmd_table* i;
  for (i = cmds; i->cmd; ++i)
    if (strncmp (line, i->cmd, i->sz) == 0)
      break;

  if (!i->cmd)
    {
      rxpd_log (base, LOG_ERR, "no command\n");
      rxpd_buffer_printf (&self->out, "#ERROR: no command\n");
      goto end;
    }

  if (!rxpd_connection_check_policy (self, line))
    {
      rxpd_buffer_printf (&self->out, "#ERROR: access denied\n");
      goto end;
    }

  if (line[i->sz])
    {
      // filename provided
      self->file = (struct rxpd_file*) psplay_find (&base->files, &line[i->sz]);

      if (!self->file)
        {
          self->file = rxpd_file_new (base, &line[i->sz]);
          if (!self->file)
            {
              rxpd_buffer_printf (&self->out, "#ERROR: illegal filename\n");
              goto end;
            }
          if (rxpd_file_load (self->file))
            rxpd_log (base, LOG_INFO, "autoloaded '%s'\n", &line[i->sz]);
          else
            rxpd_log (base, LOG_WARNING, "could not autoload '%s'\n", &line[i->sz]);
        }
    }

  // dispatch
  switch (i->nr)
    {
#define RXPD_CMD(cmd, _)                        \
case RXPD_CMD_##cmd:                            \
   rxpd_connection_cmd_##cmd (self); break;

      RXPD_COMMANDS
#undef RXPD_CMD
    }

 end:
  pth_cleanup_pop (TRUE);
  return NULL;
}

