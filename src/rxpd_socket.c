/*
    rxpd_socket.c - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rxpd.h"

struct rxpd_socket*
rxpd_socket_new_tcp4 (struct rxpd_base* base, const char* addr, unsigned short port)
{
  struct rxpd_socket* self;
  self = rxpd_malloc (sizeof (struct rxpd_socket));

  self->base = base;

  llist_init (&self->node);

  errno = 0;

  self->fd = socket (PF_INET, SOCK_STREAM, 0);
  if (self->fd == -1)
    goto esocket;

  struct sockaddr_in listen_addr;
  memset (&listen_addr, 0, sizeof (listen_addr));

  listen_addr.sin_family = AF_INET;
  if (addr)
    {
      if (inet_aton (addr, &listen_addr.sin_addr) == 0)
        goto esocket;
    }
  else
    listen_addr.sin_addr.s_addr = INADDR_ANY;

  listen_addr.sin_port = htons(port);

  static int yes = 1;
  if (setsockopt (self->fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
    goto esocket;

  if (bind (self->fd, (struct sockaddr*)&listen_addr, sizeof (listen_addr)) == -1)
    goto esocket;

  if (listen (self->fd, 20) == -1)
    goto esocket;

  self->rxpd_socket_addr = rxpd_socket_tcp4addr;

  self->accepter = NULL;
  self->ev = NULL; 
  llist_init (&self->connections);
  llist_insert_tail (&base->sockets, &self->node);

  rxpd_log (base, LOG_INFO, "Listening on tcp4:%d\n", port);
  return self;

 esocket:
  rxpd_die ("failed creating socket: %s\n", strerror(errno));
  /* free (self); rxpd_die never returns */
  return NULL;
}

int
rxpd_socket_tcp4addr (struct rxpd_connection* conn, char* dst, const char* pfx, size_t size)
{
  struct sockaddr_in peer;
  socklen_t len = sizeof (peer);
  getpeername (conn->fd, (struct sockaddr*)&peer, &len);

  char* addr;
  addr = inet_ntoa (peer.sin_addr);
  if (sizeof (":tcp4:") + strlen (pfx) + strlen (addr) > size)
    return 0;

  strcat (dst, pfx);
  strcat (dst, ":tcp4:");
  strcat (dst, addr);
  return 1;
}

void
rxpd_socket_delete (struct rxpd_socket* self)
{
  if (self)
    {
      if (self->fd != -1)
        close (self->fd);
      llist_unlink (&self->node);
      free (self);
    }
}

static void
rxpd_socket_cleanup (void* ptr)
{
  struct rxpd_socket* self = ptr;
  rxpd_log (self->base, LOG_DEBUG, "socket thread canceled\n");
  pth_event_free (self->ev, PTH_FREE_ALL);
  close (self->fd);
  self->fd = -1;
  LLIST_WHILE_HEAD (&self->connections, m)
    {
      struct rxpd_connection* conn = (struct rxpd_connection*)m;
      rxpd_connection_cancel (conn);
    }
}

struct rxpd_socket*
rxpd_socket_join (struct rxpd_socket* self)
{
  pth_join (self->accepter, NULL);
  self->accepter = NULL;
  return self;
}

struct rxpd_socket*
rxpd_socket_spawn (struct rxpd_socket* self)
{
  if (self)
    {
      if (self->accepter)
        rxpd_die ("socket thread already spawned\n");

      self->accepter = pth_spawn (PTH_ATTR_DEFAULT, rxpd_socket_accept, self);

      if (!self->accepter)
        rxpd_die ("failed spawning thread\n");
    }
  return self;
}

struct rxpd_socket*
rxpd_socket_cancel (struct rxpd_socket* self)
{
  pth_cancel (self->accepter);
  return self;
}

void *
rxpd_socket_accept (void* ptr)
{
  struct rxpd_socket* self = ptr;

  self->ev = pth_event (PTH_EVENT_FD|PTH_UNTIL_FD_READABLE, self->fd);

  pth_cleanup_push (rxpd_socket_cleanup, ptr);

  while (pth_wait (self->ev))
    {

      int fd = pth_accept (self->fd, NULL, 0);
      if (fd == -1)
        rxpd_die ("error accepting connection\n");

      struct rxpd_connection* conn = rxpd_connection_new (self, fd);

      char buf[512];
      *buf = '\0';

      if (self->rxpd_socket_addr (conn, buf, "", 511))
        {
          rxpd_log (self->base, LOG_INFO, "incoming connection '%s'\n", buf);
          rxpd_connection_spawn (conn);
        }
      else
        rxpd_log (self->base, LOG_ERR, "illegal hostname\n", buf);
    }

  /* not yet reached code */
  rxpd_log (self->base, LOG_NOTICE, "closed\n");
  pth_cleanup_pop (TRUE);
  return NULL;
}

