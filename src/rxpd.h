/*
    rxpd.h - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
#ifndef RXPD_H
#define RXPD_H

#define _GNU_SOURCE

#include "llist.h"
#include "psplay.h"

#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <regex.h>
#include <syslog.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pth.h>
#include <time.h>
#include <netdb.h>
#include <pwd.h>
#include <grp.h>

#define RXPD_COMMANDS                                           \
  RXPD_CMD(CHECK,       "data against regular expressions")     \
  RXPD_CMD(APPEND,      "new rules to a list")                  \
  RXPD_CMD(PREPEND,     "new rules in front of a list")         \
  RXPD_CMD(REMOVE,      "rules from a list")                    \
  RXPD_CMD(REPLACE,     "a rule in a list with new rules")      \
  RXPD_CMD(CLEAR,       "list rules in memory")                 \
  RXPD_CMD(DELETE,      "a list from disk")                     \
  RXPD_CMD(LOAD,        "a list from disk")                     \
  RXPD_CMD(SAVE,        "a list to disk")                       \
  RXPD_CMD(FETCH,       "a list from a remote server")          \
  RXPD_CMD(UPDATE,      "atimes from other lists")              \
  RXPD_CMD(MERGE,       "new rules from from other lists")      \
  RXPD_CMD(EXPIRE,      "aged rules from a list")               \
  RXPD_CMD(FILTER,      "rules in a list")                      \
  RXPD_CMD(DUMP,        "rules in a list")                      \
  RXPD_CMD(DUMPH,       "rules in a list, human readable atime")\
  RXPD_CMD(LIST,        "all existing lists")                   \
  RXPD_CMD(VERSION,     "of this rxpd is "PACKAGE_VERSION)      \
  RXPD_CMD(HELP,        "is what you see right now")            \
  RXPD_CMD(SHUTDOWN,    "the daemon")

#define RXPD_CMD(cmd, _) RXPD_CMD_##cmd,
enum rxpd_cmd_e {RXPD_COMMANDS};
#undef RXPD_CMD

/*
 * characters which are absolutely not allowed in rule filenames
 * for finer control use policies, note that this are arbitary
 * decisions also reserving some chars for later extension
 */
#define RXPD_FILE_ILG_CHARS "&?<>|: \t\n\r*?\\"

/*
 * Call a cooperative pth_yield every this much expensive interations
 * not implemented yet
 * A higher number favors throughput, lower number improves latency
 */
#define RXPD_YIELD_EVERY 500

#define RXPD_PREFIXCMP(str, pfx) (!strncmp (str, pfx, sizeof (pfx)-1))

struct rxpd_base;
struct rxpd_file;
struct rxpd_rule;
struct rxpd_socket;
struct rxpd_buffer;
struct rxpd_connection;

struct rxpd_base
{
  char* basedir;

  int verbosity;
  int regflags;
  int daemonize;
  const char* locale;
  const char* user;

  struct rxpd_file* policy;

  pth_t main;

  /* we have few operations which operate on two or more lists (UPDATE, MERGE, FILTER),
     just using this big lock to serialize these.
     Other strategies like timed_lock/rollback might be implemented later
  */
  pth_mutex_t nest_lock;

  // TODO
  //FILE* -l log      log hits to logfile

  psplayroot files;
  llist sockets;
};

struct rxpd_base*
rxpd_init (void);

void
rxpd_destroy (void);

void
rxpd_log (struct rxpd_base*, int level, const char* fmt, ...);

void
rxpd_fail (struct rxpd_base*, const char* fmt, ...);

void
rxpd_die (const char* fmt, ...);

void*
rxpd_malloc (size_t size);

char*
rxpd_strdup (const char* str);

void
rxpd_enter_personality (struct rxpd_base* self);

void
rxpd_signalloop (struct rxpd_base* self);

//
struct rxpd_rule
{
  llist node;
  char* string;
  time_t atime;
  struct rxpd_base* base;
  regex_t rx;
};

struct rxpd_rule*
rxpd_rule_new (const char* buf, struct rxpd_base* base);

struct rxpd_rule*
rxpd_rule_copy (const struct rxpd_rule* src);

struct rxpd_rule*
rxpd_rule_activate (struct rxpd_rule* self);

struct rxpd_rule*
rxpd_rule_comment (struct rxpd_rule* self, const char* comment);

void
rxpd_rule_delete (struct rxpd_rule* self);



//
struct rxpd_file
{
  psplay node;          // key points to basename part of filename
  //TODO later     struct stat last_stat;
  struct rxpd_base* base;
  pth_rwlock_t lock;
  llist rules;
};

struct rxpd_file*
rxpd_file_new (struct rxpd_base* base, const char* filename);

void
rxpd_file_delete (struct rxpd_file* file);

void
rxpd_file_remove (struct rxpd_file* file);

struct rxpd_file*
rxpd_file_rules_delete (struct rxpd_file* self);

int
rxpd_file_load (struct rxpd_file* self);

int
rxpd_file_save (struct rxpd_file* self, int force);

struct rxpd_file*
rxpd_file_dump (struct rxpd_file* self, struct rxpd_buffer* out, const char* fmt);

int
rxpd_file_cmp (const void* A, const void* B);


//

struct rxpd_socket
{
  llist node;
  int fd;
  pth_event_t ev;
  struct rxpd_base* base;
  pth_t accepter;
  int (*rxpd_socket_addr)(struct rxpd_connection* conn, char* dst, const char* pfx, size_t size);
  llist connections;
};


struct rxpd_socket*
rxpd_socket_new_tcp4 (struct rxpd_base* base, const char* addr, unsigned short port);

int
rxpd_socket_tcp4addr (struct rxpd_connection* conn, char* dst, const char* pfx, size_t size);

//TODO
//struct rxpd_socket*
//rxpd_socket_new_unix (struct rxpd_base* base, const char* name);

void
rxpd_socket_delete (struct rxpd_socket* self);

void *
rxpd_socket_accept (void* ptr);

struct rxpd_socket*
rxpd_socket_spawn (struct rxpd_socket* self);

struct rxpd_socket*
rxpd_socket_join (struct rxpd_socket* self);

struct rxpd_socket*
rxpd_socket_cancel (struct rxpd_socket* self);

//

enum rxpd_buffer_state_e
  {
    RXPD_OK,            // operational
    RXPD_EOF,           // connection closed
    RXPD_ERROR          // some other error
  };

struct rxpd_buffer
{
  int fd;
  enum rxpd_buffer_state_e state;
  char* eol;
  char* eob;
  char buffer[4096];
};

struct rxpd_buffer*
rxpd_buffer_init (struct rxpd_buffer* self, int fd);

char*
rxpd_buffer_readline (struct rxpd_buffer* self);

int
rxpd_buffer_printf (struct rxpd_buffer* self, const char* fmt, ...);

inline static enum rxpd_buffer_state_e
rxpd_buffer_state (struct rxpd_buffer* self)
{
  return self->state;
}


//
struct rxpd_connection
{
  llist node;
  int fd;
  pth_t connecter;
  struct rxpd_file* file;
  struct rxpd_socket* socket;
  char* tmp_str;
  llist tmp_list;

  struct rxpd_buffer in;
  struct rxpd_buffer out;
};


struct rxpd_connection*
rxpd_connection_new (struct rxpd_socket* socket, int fd);

void
rxpd_connection_delete (struct rxpd_connection* self);

struct rxpd_connection*
rxpd_connection_spawn (struct rxpd_connection* self);

struct rxpd_connection*
rxpd_connection_cancel (struct rxpd_connection* self);

int
rxpd_connection_readline (struct rxpd_connection* self);

int
rxpd_connection_check_policy (struct rxpd_connection* self, char* line);

void*
rxpd_connection_parse_cmd (void* ptr);

/* generate prototypes for each defined command */
#define RXPD_CMD(cmd, _) void rxpd_connection_cmd_##cmd (struct rxpd_connection* self);
RXPD_COMMANDS
#undef RXPD_CMD


#endif
