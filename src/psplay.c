/*
    psplay.c - probabilistic splay tree

  Copyright (C)
    2004, 2005, 2006,   Christian Thaeter <chth@gmx.net>
  Copyright (C)         CinelerraCV
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "psplay.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
  probabilistic distribution as n/255 chance of splaying
*/
#ifndef PSPLAY_PROB_ZIG
/* already near root, only approx 6% splay probability */
#define PSPLAY_PROB_ZIG 16
#endif
/* approx 63% splay probability */
#ifndef PSPLAY_PROB_ZIGZIG
#define PSPLAY_PROB_ZIGZIG 160
#endif
/* zigzag reduces the tree by 1 level, approx 88% splay probability */
#ifndef PSPLAY_PROB_ZIGZAG
#define PSPLAY_PROB_ZIGZAG 224
#endif



/* simple prng with 2^31-1 cycle */
static inline uint32_t psplay_fast_prng ()
{
  static uint32_t rnd=0xbabeface;
  return rnd = rnd<<1 ^ ((rnd >> 30) & 1) ^ ((rnd>>2) & 1);
}


PSplayroot
psplay_init_root (PSplayroot self, psplay_cmp_t cmp, psplay_delete_t delete)
{
  if (self)
    {
      self->tree = NULL;
      self->cmp = cmp;
      self->delete = delete;
      self->elem_cnt = 0;
    }
  return self;
}


PSplayroot
psplay_destroy_root (PSplayroot self)
{
  if (self) while (self->tree)
    {
      PSplay n = psplay_remove (self, self->tree);
      if (self->delete)
        self->delete (n);
    }
  return self;
}






//
inline void
psplay_splay (PSplay* root, PSplay node)
{
  while (node->up)
    {
      PSplay p = node->up;              // parent
      PSplay g = p?p->up:NULL;          // grandparent

      if (g)
        {
          if (p == g->left)
            {
              // zig..
              if (node == p->left)
                {
                  // zigzig
                  if ((psplay_fast_prng()&0xFF) >= PSPLAY_PROB_ZIGZIG)
                    return;

                  g->left = p->right;
                  if (g->left) g->left->up = g;
                  p->right = g;

                  p->left = node->right;
                  if (p->left) p->left->up = p;
                  node->right = p;

                  node->up = g->up;
                  g->up = p;
                }
              else
                {
                  // zigzag
                  if ((psplay_fast_prng()&0xFF) >= PSPLAY_PROB_ZIGZAG)
                    return;

                  p->right = node->left;
                  if (p->right) p->right->up = p;
                  node->left = p;

                  g->left = node->right;
                  if (g->left) g->left->up = g;
                  node->right = g;

                  node->up = g->up;
                  g->up = node;
                }
            }
          else
            {
              // zag..
              if (node == p->left)
                {
                  // zagzig
                  if ((psplay_fast_prng()&0xFF) >= PSPLAY_PROB_ZIGZAG)
                    return;

                  p->left = node->right;
                  if (p->left) p->left->up = p;
                  node->right = p;

                  g->right = node->left;
                  if (g->right) g->right->up = g;
                  node->left = g;

                  node->up = g->up;
                  g->up = node;
                }
              else
                {
                  // zagzag
                  if ((psplay_fast_prng()&0xFF) >= PSPLAY_PROB_ZIGZIG)
                    return;

                  g->right = p->left;
                  if (g->right) g->right->up = g;
                  p->left = g;

                  p->right = node->left;
                  if (p->right) p->right->up = p;
                  node->left = p;

                  node->up = g->up;
                  g->up = p;
                }
            }

          p->up = node;
          if (node->up)
            {
              if (node->up->left == g)
                node->up->left = node;
              else
                node->up->right = node;
            }
          else
            {
              *root = node;
            }
        }
      else
        {
          if (p)
            {
              if ((psplay_fast_prng()&0xFF) >= PSPLAY_PROB_ZIG)
                return;

              if (node == p->left)
                {
                  // zig
                  p->left = node->right;
                  if (p->left) p->left->up = p;
                  node->right = p;
                }
              else
                {
                  // zag
                  p->right = node->left;
                  if (p->right) p->right->up = p;
                  node->left = p;
                }
              p->up = node;
              node->up = NULL;
              *root = node;
            }
        }
    }
}


PSplay
psplay_new (void * key)
{
  return psplay_init (malloc (sizeof (psplay)), key);
}


PSplay
psplay_init (PSplay self, const void* key)
{
  if (self)
    {
      self->key = key;
      self->up = self->left = self->right = NULL;
    }
  return self;
}

void
psplay_delete (PSplay node)
{
  free (node);
};


PSplay
psplay_insert (PSplayroot root, PSplay node)
{
  PSplay n = root->tree;

  if (!n)
    root->tree = node;
  else
    {
      while (n != node)
        {
          int c;
          c = root->cmp (node->key, n->key);

          if (c < 0)
            {
              if (!n->left)
                {
                  n->left = node;
                  node->up = n;
                }
              n = n->left;
            }
          else if (c > 0)
            {
              if (!n->right)
                {
                  n->right = node;
                  node->up = n;
                }
              n = n->right;
            }
          else
            return NULL;
        }
      psplay_splay (&root->tree, node);
    }
  ++root->elem_cnt;
  return node;
}


PSplay
psplay_find (PSplayroot root, void* key)
{
  PSplay node = root->tree;

  while (node)
    {
      int c;
      c = root->cmp (key, node->key);

      if (c < 0)
          node = node->left;
      else if (c > 0)
          node = node->right;
      else
        {
          psplay_splay (&root->tree, node);
          break;
        }
    }
  return node;
}


PSplay
psplay_remove (PSplayroot root, PSplay node)
{
  if (!node) return NULL;

  PSplay p = node->up;
  PSplay* r;

  if (p)
    {
      if (p->left == node)
        r = &p->left;
      else
        r = &p->right;
    }
  else
    r = &root->tree;

  if (!node->left)
    {
      // only right leaf
      *r = node->right;
      if (*r)
        (*r)->up = p;
    }
  else if (!node->right)
    {
      // only left leaf
      *r = node->left;
      if (*r)
        (*r)->up = p;
    }
  else
    {
      PSplay i;
      if (psplay_fast_prng()&1) /* 50% probability removing left or right wards */
        {
          //left right
          for (i = node->left; i->right; i = i->right);

          i->right = node->right;
          i->right->up = i;
          i->up->right = i->left;

          if (i->left)
            i->left->up = i->up;

          if (node->left != i)
            {
              i->left = node->left;
              i->left->up = i;
            }
        }
      else
        {
          // right right
          for (i = node->right; i->left; i = i->left);

          i->left = node->left;
          i->left->up = i;
          i->up->left = i->right;

          if (i->right)
            i->right->up = i->up;

          if (node->right != i)
            {
              i->right = node->right;
              i->right->up = i;
            }
        }
      i->up = node->up;
      *r = i;
    }
  node->up = node->left = node->right = NULL;
  --root->elem_cnt;
  return node;
}

/*
  Walking a tree calls a 'action' three times PSPLAY_PREORDER before visiting the left subtree,
  PSPLAY_INORDER after visiting the left subtree and before the right subtree, PSPLAY_POSTORDER
  finally after visiting the right subtree. Example: For to traverse the tree in order action
  would only handle PSPLAY_INORDER.
  This action returns PSLPLAY_CONT when the traversal of the tree shall continue.
  An 'action' must not alter the tree itself but it can indicate aborting the tree traversal and
  how the current node is handled by its return value:
   PSPLAY_STOP:
     just stops the traversal
   PSPLAY_REMOVE:
     stops the traversal and removes the current node, calling the delete handler registered in root
   any other psplay_delete_t:
     stops the traversal and removes the current node, calling the returned delete handler with it
*/
const psplay_delete_t PSPLAY_CONT = (psplay_delete_t)0x0;
const psplay_delete_t PSPLAY_STOP = (psplay_delete_t)0x1;
const psplay_delete_t PSPLAY_REMOVE = (psplay_delete_t)0x2;

static int
psplay_handle (PSplayroot root, PSplay node, psplay_delete_t res)
{
  if (res == PSPLAY_CONT)
    return 1;

  if (res == PSPLAY_STOP)
    ;
  else if (res == PSPLAY_REMOVE)
    {
      psplay_remove (root, node);
      if (root->delete)
        root->delete (node);
    }
  else
    {
      psplay_remove (root, node);
      res (node);
    }
  return 0;
}


int
psplay_walk (PSplayroot root, PSplay node, psplay_action_t action, int level, void* data)
{
  if (!root->tree)
    return 1;

  if (!node)
    node = root->tree;

  psplay_delete_t res;

  res = action (node, PSPLAY_PREORDER, level, data);
  if (!psplay_handle (root, node, res))
    return 0;

  if (node->left)
    if (!psplay_walk (root, node->left, action, level+1, data))
      return 0;

  res = action (node, PSPLAY_INORDER, level, data);
  if (!psplay_handle (root, node, res))
    return 0;

  if (node->right)
    if (!psplay_walk (root, node->right, action, level+1, data))
      return 0;

  res = action (node, PSPLAY_POSTORDER, level, data);
  if (!psplay_handle (root, node, res))
    return 0;

  return 1;
}

psplay_delete_t
psplay_print_node (PSplay node, const enum psplay_order_e which, int level, void* data)
{
  FILE* fh = data;
  static char* sp = "                                        ";
  if (level>40)
    {
      if (which == PSPLAY_PREORDER)
        fprintf (fh, "%s ...\n", sp+40-level);
      return PSPLAY_CONT;
    }

  switch (which)
    {
    case PSPLAY_PREORDER:
      fprintf (fh, "%s%p\n", sp+40-level, node);
      fprintf (fh, "%skey %p (%.4s)\n", sp+40-level, (char*)node->key, node->key?(char*)node->key:"NULL");
      fprintf (fh, "%sup %p\n", sp+40-level, node->up);
      fprintf (fh, "%sleft %p\n", sp+40-level, node->left);
      break;
    case PSPLAY_INORDER:
      fprintf (fh, "%sright %p\n", sp+40-level, node->right);
      break;
    case PSPLAY_POSTORDER:
      break;
    }

  return PSPLAY_CONT;
}

void
psplay_dump (PSplayroot root, FILE* dest)
{
  psplay_walk (root, NULL, psplay_print_node, 0, dest);
  fprintf (dest, "\n");
}

int my_cmp (const void* a, const void* b)
{
  return strcmp (a, b);
}


#ifdef PSPLAY_TESTMAIN
int
main ()
{
  psplayroot root = PSPLAYROOT_INITIALIZER(my_cmp, NULL);
  psplay_init_root (&root, my_cmp, psplay_delete);

  psplay_insert (&root, psplay_new("foo"));
  psplay_insert (&root, psplay_new("bar"));
  psplay_insert (&root, psplay_new("baz"));
  psplay_insert (&root, psplay_new("test"));
  psplay_insert (&root, psplay_new("pap"));
  psplay_insert (&root, psplay_new("qux"));

  psplay_dump (&root, stdout);

  PSplay f = psplay_find (&root, "baz");
  printf ("found %p (%.4s)\n", f, f->key?(char*)f->key:"NULL");
  psplay_dump (&root, stdout);

  f = psplay_find (&root, "test");
  printf ("found %p (%.4s)\n", f, f->key?(char*)f->key:"NULL");
  psplay_dump (&root, stdout);

  f = psplay_find (&root, "test");
  printf ("found %p (%.4s)\n", f, f->key?(char*)f->key:"NULL");
  psplay_dump (&root, stdout);

  f = psplay_find (&root, "foo");
  printf ("found %p (%.4s)\n", f, f->key?(char*)f->key:"NULL");
  psplay_dump (&root, stdout);


  psplay_delete (psplay_remove (&root, root.tree));
  psplay_dump (&root, stdout);

  psplay_delete (psplay_remove (&root, root.tree));
  psplay_dump (&root, stdout);

  psplay_delete (psplay_remove (&root, root.tree));
  psplay_dump (&root, stdout);

  printf ("destroying\n");
  psplay_destroy_root (&root);
  psplay_dump (&root, stdout);

  psplay_delete (psplay_remove (&root, root.tree));
  psplay_dump (&root, stdout);

  psplay_delete (psplay_remove (&root, root.tree));
  psplay_dump (&root, stdout);

  psplay_delete (psplay_remove (&root, root.tree));
  psplay_dump (&root, stdout);
  return 0;
}
#endif
