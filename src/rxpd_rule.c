/*
    rxpd_rule.c - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rxpd.h"

struct rxpd_rule*
rxpd_rule_new (const char* buf, struct rxpd_base* base)
{
  struct rxpd_rule* self = rxpd_malloc (sizeof (struct rxpd_rule));

  llist_init (&self->node);
  self->atime = (time_t)-1;
  self->base = base;

  if (*buf != '#')
    {
      int err;
      char* namestart = strchr (buf, ':');
      char* rxstart = namestart? strchr (namestart+1, ':') : NULL;

      if (!rxstart || *(rxstart-1) == '>')
          self->string = rxpd_strdup ("#ERROR: Syntax error, line was neither a comment nor a rule");
      else
        {
          if (namestart != buf)
            {
              /* atime given */
              self->atime = atoi (buf);
              if (!self->atime)
                /* atime was zero or not set */
                self->atime = time (NULL)-1;
            }

          err = regcomp (&self->rx, rxstart+1, base->regflags|REG_EXTENDED|REG_NOSUB);

          if (!err)
            self->string = rxpd_strdup (namestart+1);
          else
            {
              char ebuf[256];
              size_t len = regerror (err, NULL, ebuf, 256);
              self->string = rxpd_malloc (len + strlen(namestart+1) + 14);
              strcpy (self->string, "#ERROR: ");
              strcat (self->string, ebuf);
              strcat (self->string, " in '");
              strcat (self->string, namestart+1);
              strcat (self->string, "'");
            }
        }
    }
  else
    self->string = rxpd_strdup (buf);

  return self;
}

struct rxpd_rule*
rxpd_rule_copy (const struct rxpd_rule* src)
{
  struct rxpd_rule* self = rxpd_malloc (sizeof (struct rxpd_rule));

  llist_init (&self->node);
  self->string = rxpd_strdup (src->string);
  self->atime = src->atime;

  if (*self->string != '#')
    {
      int err;
      char* rxstart = strchr (self->string, ':');

      err = regcomp (&self->rx, rxstart+1, src->base->regflags|REG_EXTENDED|REG_NOSUB);
      if (err)
        rxpd_die ("unexpected regcomp error\n");
    }

  return self;
}

struct rxpd_rule*
rxpd_rule_activate (struct rxpd_rule* self)
{
  if (self)
    {
      char* buf;
      if (self->string[0] == '#' && (buf = strstr (self->string, ": ")+2) && *buf)
        {
          if (*buf != '#')
            {
              char* namestart = strchr (buf, ':');
              char* rxstart = namestart? strchr (namestart+1, ':') : NULL;

              if (!rxstart)
                return NULL;

              if (regcomp (&self->rx, rxstart+1, self->base->regflags|REG_EXTENDED|REG_NOSUB))
                return NULL;

              free (self->string);
              self->string = rxpd_strdup (namestart+1);

              if (namestart != buf)
                {
                  /* atime given */
                  self->atime = atoi (buf);
                  if (!self->atime)
                    /* atime was zero or not set */
                    self->atime = time (NULL)-1;
                }
            }
          else
            self->string = rxpd_strdup (buf);
        }
      else
        return NULL;
    }
  return self;
}

struct rxpd_rule*
rxpd_rule_comment (struct rxpd_rule* self, const char* comment)
{
  if (self)
    {
      int len;
      if (self->atime != -1)
        len = snprintf (NULL, 0, "#%s: %ld:%s", comment, self->atime, self->string);
      else if (self->string[0] != '#')
        len = snprintf (NULL, 0, "#%s: :%s", comment, self->string);
      else
        len = snprintf (NULL, 0, "#%s: %s", comment, self->string);

      if (len < 0)
        return NULL;

      char* dst = rxpd_malloc (len+1);

      if (self->atime != -1)
        snprintf (dst, len+1, "#%s: %ld:%s", comment, self->atime, self->string);
      else if (self->string[0] != '#')
        snprintf (dst, len+1, "#%s: :%s", comment, self->string);
      else
        snprintf (dst, len+1, "#%s: %s", comment, self->string);

      if (self->string[0] != '#')
        regfree (&self->rx);

      self->atime = -1;

      free (self->string);
      self->string = dst;
    }
  return self;
}

void
rxpd_rule_delete (struct rxpd_rule* rule)
{
  if (rule)
    {
      llist_unlink (&rule->node);
      if (rule->string[0] != '#')
        regfree (&rule->rx);
      free (rule->string);
      free(rule);
    }
}


