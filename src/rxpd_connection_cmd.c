/*
    rxpd_connection_cmd.c - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rxpd.h"

#define RXPD_FILENAME_REQUIRED                                          \
  do {                                                                  \
    if (!self->file)                                                    \
      {                                                                 \
        rxpd_buffer_printf (&self->out, "#ERROR: missing filename\n");  \
        return;                                                         \
      }                                                                 \
  } while (0)


static char*
rxpd_connection_do_CHECK (struct rxpd_connection* self, char* line, struct rxpd_file* file)
{
  char* ret = NULL;

  // TODO implement RXPD_YIELD_EVERY
  pth_rwlock_acquire (&file->lock, PTH_RWLOCK_RD, FALSE, NULL);
  LLIST_FOREACH (&file->rules, n)
    {
      struct rxpd_rule* rule = (struct rxpd_rule*)n;
      if (rule->string[0] != '#')
        {
          if (regexec (&rule->rx, line, 0, NULL, 0) == 0)
            {
              if (rule->atime != (time_t) -1)
                time (&rule->atime);

              ret = rule->string;

              if (*ret == '>')
                {
                  struct rxpd_base* base = self->socket->base;

                  size_t sublen =
                    (size_t)(strchr (&rule->string[1], ':') - &rule->string[1]);

                  char* subname =
                    alloca (strlen (file->node.key) + sublen + 1);
                  *subname = '\0';

                  strcat (subname, file->node.key);
                  strncat (subname, &rule->string[1], sublen);

                  struct rxpd_file* subfile = (struct rxpd_file*) psplay_find (&base->files, subname);
                  if (!subfile)
                    {
                      subfile = rxpd_file_new (base, subname);
                      if (!subfile)
                        {
                          rxpd_buffer_printf (&self->out, "#ERROR: illegal filename\n");
                          break;
                        }
                      if (rxpd_file_load (subfile))
                        rxpd_log (base, LOG_INFO, "autoloaded '%s'\n", subname);
                      else
                        rxpd_log (base, LOG_WARNING, "could not autoload '%s'\n", subname);
                    }

                  rxpd_log (base, LOG_DEBUG, "going to sublist '%s'\n", subname);
                  ret = rxpd_connection_do_CHECK (self, line, subfile);
                }

              if (ret)
                break;
            }
        }
    }
  pth_rwlock_release (&file->lock);
  return ret;
}


void
rxpd_connection_cmd_CHECK (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  char* line;
  while ((line = rxpd_buffer_readline (&self->in)))
    {
      if (*line == '\0')
        {
          rxpd_buffer_printf (&self->out, "#OK:\n");
        }
      else
        {
          char* match = rxpd_connection_do_CHECK (self, line, self->file);

          if (match)
            rxpd_buffer_printf (&self->out, "%s\n", match);
        }
    }

  if (rxpd_buffer_state (&self->in) == RXPD_ERROR)
    rxpd_buffer_printf (&self->out, "#ERROR:\n");
}


static void
rxpd_connection_APPEND_PREPEND_helper (struct rxpd_connection* self)
{
  char* line;

  while ((line = rxpd_buffer_readline (&self->in)))
    {
      if (*line && !RXPD_PREFIXCMP (line, "!EXIT"))
        {
          struct rxpd_rule* rule;

          rule = rxpd_rule_new (line, self->socket->base);
          llist_insert_tail (&self->tmp_list, &rule->node);
        }
      else
        break;  /* exit at empty line, error or whatever */
    }

  if (rxpd_buffer_state (&self->in) == RXPD_ERROR)
    rxpd_buffer_printf (&self->out, "#ERROR:\n");
  else
    rxpd_buffer_printf (&self->out, "#OK:\n");
}

void
rxpd_connection_cmd_APPEND (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);

  rxpd_connection_APPEND_PREPEND_helper (self);
  llist_insertlist_prev (&self->file->rules, &self->tmp_list);

  pth_rwlock_release (&self->file->lock);
}

void
rxpd_connection_cmd_PREPEND (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);

  rxpd_connection_APPEND_PREPEND_helper (self);
  llist_insertlist_next (&self->file->rules, &self->tmp_list);

  pth_rwlock_release (&self->file->lock);
}

void
rxpd_connection_cmd_REMOVE (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  char* line;
  while ((line = rxpd_buffer_readline (&self->in)))
    {
      if (*line && !RXPD_PREFIXCMP (line, "!EXIT"))
        {
          pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);
          LLIST_FOREACH (&self->file->rules, n)
            {
              struct rxpd_rule* rule = (struct rxpd_rule*)n;
              if (strcmp (rule->string, line) == 0)
                {
                  LList tmp = llist_prev (n);
                  rxpd_rule_delete (rule);
                  n = tmp;
                  rxpd_buffer_printf (&self->out, "#OK:\n");
                  goto done;
                }
            }
          rxpd_buffer_printf (&self->out, "#ERROR: line not found\n");
        done:
          pth_rwlock_release (&self->file->lock);
        }
      else
        break;  /* exit at empty line, error or whatever */
    }

  if (rxpd_buffer_state (&self->in) == RXPD_ERROR)
    rxpd_buffer_printf (&self->out, "#ERROR:\n");
}



static int
rxpd_connection_do_REPLACE (struct rxpd_connection* self)
{
  struct rxpd_rule* rule;

  LLIST_FOREACH (&self->file->rules, n)
    {
      rule = (struct rxpd_rule*)n;
      if (strcmp (rule->string, self->tmp_str) == 0)
        goto found;
    }
  return 0;

 found:
  llist_insertlist_next (&rule->node, &self->tmp_list);
  rxpd_rule_delete (rule);

  free (self->tmp_str);
  self->tmp_str = NULL;
  return 1;
}

void
rxpd_connection_cmd_REPLACE (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);

  char* line;
  while ((line = rxpd_buffer_readline (&self->in)))
    {
      if (*line && !RXPD_PREFIXCMP (line, "!EXIT"))
        {
          if (self->tmp_str)
            {
              struct rxpd_rule* rule;
              rule = rxpd_rule_new (line, self->socket->base);
              if (rule)
                llist_insert_tail (&self->tmp_list, &rule->node);
              else
                rxpd_buffer_printf (&self->out, "#ERROR: illegal rule '%s'\n", line);
            }
          else
            self->tmp_str = rxpd_strdup (line);
        }
      else
        break;  /* exit at empty line, error or whatever */
    }

  if (rxpd_buffer_state (&self->in) == RXPD_ERROR)
    rxpd_buffer_printf (&self->out, "#ERROR:\n");

  if (rxpd_connection_do_REPLACE (self))
    rxpd_buffer_printf (&self->out, "#OK:\n");
  else
    rxpd_buffer_printf (&self->out, "#ERROR: no rule matching '%s'\n", self->tmp_str);

  pth_rwlock_release (&self->file->lock);
}

void
rxpd_connection_cmd_LOAD (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  if (rxpd_file_load (self->file))
    rxpd_buffer_printf (&self->out, "#OK:\n");
  else
    rxpd_buffer_printf (&self->out, "#ERROR: loading file '%s'\n", (const char*)self->file->node.key);
}

void
rxpd_connection_cmd_SAVE (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  if (rxpd_file_save (self->file, 1))
    rxpd_buffer_printf (&self->out, "#OK:\n");
  else
    rxpd_buffer_printf (&self->out, "#ERROR: saving file '%s'\n", (const char*)self->file->node.key);
}


void
rxpd_connection_cmd_CLEAR (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  rxpd_file_rules_delete (self->file);
  rxpd_buffer_printf (&self->out, "#OK:\n");
}

void
rxpd_connection_cmd_DELETE (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  rxpd_file_remove (self->file);
  rxpd_buffer_printf (&self->out, "#OK:\n");
}

void
rxpd_connection_cmd_FETCH (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  struct rxpd_base* base = self->socket->base;

  char* line;
  line = rxpd_buffer_readline (&self->in);
  if (*line)
    {
      /* parse line */
      char* list = strrchr (line, ':');
      if (!list)
        goto esyntax;

      *list = '\0';
      ++ list;

      char* port = strrchr (line, ':');
      if (!port)
        goto esyntax;

      *port = '\0';
      ++ port;

      struct addrinfo* addrs = NULL;
      int aierr;

      /* resolve peer */
      aierr = getaddrinfo (line, port, NULL, &addrs);
      if (aierr)
        {
          rxpd_buffer_printf (&self->out, "#ERROR: resolving '%s': %s\n", line, gai_strerror (aierr));
          return;
        }

      rxpd_log (base, LOG_INFO, "fetching list '%s' from '%s(%s)' at port '%s' to '%s'\n",
                list,
                line,
                inet_ntoa (((struct sockaddr_in*)addrs->ai_addr)->sin_addr),
                port,
                self->file->node.key);

      /* establish connection */
      int fd = socket (addrs->ai_family, addrs->ai_socktype, addrs->ai_protocol);
      if (fd == -1 || connect (fd, addrs->ai_addr, addrs->ai_addrlen))
        {
          freeaddrinfo (addrs);
          rxpd_buffer_printf (&self->out, "#ERROR: error connecting '%s': %s\n", line, strerror (errno));
          return;
        }
 
      freeaddrinfo (addrs);

      /* send DUMP command and fetch data */
      struct rxpd_buffer in;
      struct rxpd_buffer out;
      rxpd_buffer_init (&in, fd);
      rxpd_buffer_init (&out, fd);

      rxpd_buffer_printf (&out, "DUMP:%s\n", list);

      char* line;
      while ((line = rxpd_buffer_readline (&in)))
        {
          if (*line && !RXPD_PREFIXCMP (line, "!EXIT"))
            {
              struct rxpd_rule* rule;
              rule = rxpd_rule_new (line, base);
              if (!rule)
                rxpd_die ("rule creation failed on '%s'\n", line);

              llist_insert_tail (&self->tmp_list, &rule->node);
            }
          else
            rxpd_log (base, LOG_INFO, "purge remote side error '%s'\n", line);
        }
      rxpd_log (base, LOG_DEBUG, "finished dump\n");

      close (fd);

      pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);
      rxpd_file_rules_delete (self->file);
      llist_insertlist_next (&self->file->rules, &self->tmp_list);
      pth_rwlock_release (&self->file->lock);

      rxpd_buffer_printf (&self->out, "#OK:\n");
    }
  else
    {
    esyntax:
      rxpd_buffer_printf (&self->out, "#ERROR: syntax error\n");
    }
}

void
rxpd_connection_cmd_DUMP (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;
  rxpd_file_dump (self->file, &self->out, NULL);
}

void
rxpd_connection_cmd_DUMPH (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;
  rxpd_file_dump (self->file, &self->out, "%a, %d. %b. %Y, %T");
}


/*
  build a temporary list to make psplay walking lockfree under pth
*/
struct list_record
{
  llist node;
  char* name;
};

static psplay_delete_t
walk_LIST (PSplay node, const enum psplay_order_e which, int level, void* data)
{
  (void) level;
  struct rxpd_file* file = (struct rxpd_file*) node;
  struct rxpd_connection* conn = (struct rxpd_connection*) data;

  char buf[4096] = "DUMP:";
  buf[4095] = '\0';

  if (which == PSPLAY_INORDER)
    {
      strncpy (buf+sizeof("DUMP:")-1, file->node.key, 4096 - sizeof("DUMP:"));

      if (rxpd_connection_check_policy (conn, buf))
        {
          struct list_record* rec = rxpd_malloc (sizeof (struct list_record));
          rec->name = rxpd_strdup (file->node.key);
          llist_init (&rec->node);
          llist_insert_tail (&conn->tmp_list, &rec->node);
        }
    }
  return PSPLAY_CONT;
}


void
rxpd_connection_cmd_LIST (struct rxpd_connection* self)
{
  struct rxpd_base* base = self->socket->base;

  if (psplay_isempty_root (&base->files))
    rxpd_buffer_printf (&self->out, "#OK:\n");
  else
    {
      psplay_walk (&base->files, NULL, walk_LIST, 0, self);

      LLIST_WHILE_HEAD (&self->tmp_list, n)
        {
          struct list_record* rec = (struct list_record*)n;
          rxpd_buffer_printf (&self->out, "%s\n", rec->name);
          llist_unlink (n);
          free (rec->name);
          free (n);
        }
    }
}

void
rxpd_connection_cmd_SHUTDOWN (struct rxpd_connection* self)
{
  /* not pth_raise, we don't want to schedule here! */
  raise (SIGINT);
  rxpd_buffer_printf (&self->out, "#OK:\n");
}


void
rxpd_connection_cmd_VERSION (struct rxpd_connection* self)
{
  rxpd_buffer_printf (&self->out, PACKAGE_STRING "\n#\n"
                      "# Copyright (C)\n"
                      "#   2007,               Christian Thaeter <ct@pipapo.org>\n#\n"
                      "# This is free software.  You may redistribute copies of it under the terms of\n"
                      "# the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.\n"
                      "# There is NO WARRANTY, to the extent permitted by law.\n#\n"
                      "# http://www.pipapo.org/pipawiki/RegexPolicyDaemon\n");
}

void
rxpd_connection_cmd_HELP (struct rxpd_connection* self)
{
  rxpd_buffer_printf (&self->out, "# Available commands:\n#\n");
#define RXPD_CMD(cmd, help)     rxpd_buffer_printf (&self->out, "# %s %s.\n", #cmd, help);
  RXPD_COMMANDS
#undef RXPD_CMD
    rxpd_buffer_printf (&self->out, 
                        "#\n"
                        "# general syntax is: 'COMMAND:listname\\n..data..'\n"
                        "# see http://www.pipapo.org/pipawiki/RegexPolicyDaemon\n");
}

void
rxpd_connection_cmd_EXPIRE (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  struct rxpd_base* base = self->socket->base;

  char* line = rxpd_buffer_readline (&self->in);
  if (*line)
    {
      // TODO strtol, error handling
      time_t since = time (NULL) - atoi (line);
      rxpd_log (base, LOG_INFO, "expire all entries in '%s' since %ld\n",
                (const char*) self->file->node.key, since);

      pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);

      LLIST_FOREACH (&self->file->rules, n)
        {
          struct rxpd_rule* rule = (struct rxpd_rule*)n;
          if (rule->atime != -1 && rule->atime < since)
            {
              rxpd_log (base, LOG_DEBUG, "expiring %ld:%s\n", rule->atime, rule->string);
              rxpd_buffer_printf (&self->out, "#OK: expiring '%s'\n", rule->string);
              if (!rxpd_rule_comment (rule, "EXPIRED"))
                rxpd_die ("couldn't comment rule out");
            }
        }

      pth_rwlock_release (&self->file->lock);
    }
  else
    rxpd_buffer_printf (&self->out, "#ERROR: no age given\n");
}

void
rxpd_connection_cmd_UPDATE (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  struct rxpd_base* base = self->socket->base;

  pth_mutex_acquire (&base->nest_lock, 0, NULL);
  pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);

  char* line;
  while ((line = rxpd_buffer_readline (&self->in)))
    {
      /* for each line which is a listname */
      struct rxpd_file* source;
      source = (struct rxpd_file*) psplay_find (&base->files, line);
      if (source)
        {
          pth_rwlock_acquire (&source->lock, PTH_RWLOCK_RD, FALSE, NULL);

          /* find first line which is in source and self->file, this is our start */
          LList source_itr = NULL;
          LList file_itr = NULL;
          LLIST_FOREACH (&source->rules, n)
            {
              struct rxpd_rule* rule_n = (struct rxpd_rule*)n;
              LLIST_FOREACH (&self->file->rules, m)
                {
                  struct rxpd_rule* rule_m = (struct rxpd_rule*)m;
                  if (strcmp (rule_n->string, rule_m->string) == 0)
                    {
                      source_itr = &rule_n->node;
                      file_itr = &rule_m->node;
                      rxpd_log (base, LOG_DEBUG, "starting at '%s'\n", rule_n->string);
                      goto leave_loop;
                    }
                }
            }
        leave_loop:
          if (!source_itr)
            {
              rxpd_log (base, LOG_DEBUG, "no common line found between '%s' and '%s'\n",
                        (const char*)self->file->node.key, line);
              pth_rwlock_release (&source->lock);
              break;
            }

          /* Now we go for each rule in source which has a atime find the matching rule in self->file and update its atime */
          /* This algorihm assumes that lists stay ordered (insertions/deletions are supported, but not reordering) */
          /* advantage is that the algorithm has far less complexity */
          LLIST_FORRANGE (source_itr, &source->rules, n)
            {
              struct rxpd_rule* rule_n = (struct rxpd_rule*)n;
              if (rule_n->atime != (time_t)-1)
                {
                  LLIST_FORRANGE (file_itr, &self->file->rules, m)
                    {
                      struct rxpd_rule* rule_m = (struct rxpd_rule*)m;
                      if (strcmp (rule_n->string, rule_m->string) == 0)
                        {
                          /* found the rule */
                          if (rule_m->atime != (time_t)-1 && rule_m->atime < rule_n->atime)
                            {
                              rxpd_log (base, LOG_DEBUG,
                                        "updating atime of rule '%s' from %ld to %ld\n",
                                        rule_m->string,
                                        rule_m->atime,
                                        rule_n->atime
                                        );
                              rule_m->atime = rule_n->atime;
                            }

                          file_itr = llist_next (m);
                          break;
                        }
                    }
                }
            }

          pth_rwlock_release (&source->lock);
        }
      else
        rxpd_buffer_printf (&self->out, "#ERROR: unknown file '%s'\n", line);
    }

  pth_rwlock_release (&self->file->lock);
  pth_mutex_release (&base->nest_lock);

  rxpd_buffer_printf (&self->out, "#OK:\n");
}


void
rxpd_connection_cmd_MERGE (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  struct rxpd_base* base = self->socket->base;

  pth_mutex_acquire (&base->nest_lock, 0, NULL);
  pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);

  char* line;
  while ((line = rxpd_buffer_readline (&self->in)))
    {
      /* for each line which is a listname */
      struct rxpd_file* source;
      source = (struct rxpd_file*) psplay_find (&base->files, line);
      if (source)
        {
          pth_rwlock_acquire (&source->lock, PTH_RWLOCK_RD, FALSE, NULL);

          LList source_start = llist_head (&source->rules);
          LList file_start = llist_head (&self->file->rules);
          LList source_itr = NULL;
          LList file_itr = NULL;

          /* find next line which is in source and self->file */
          while (source_itr != &source->rules)
            {
              LLIST_FORRANGE (source_start, &source->rules, n)
                {
                  LLIST_FORRANGE (file_start, &self->file->rules, m)
                    {
                      struct rxpd_rule* rule_n = (struct rxpd_rule*)n;
                      struct rxpd_rule* rule_m = (struct rxpd_rule*)m;
                      if (strcmp (rule_n->string, rule_m->string) == 0)
                        {
                          source_itr = &rule_n->node;
                          file_itr = &rule_m->node;
                          goto leave_loop;
                        }
                    }
                }
              /* not found, place the itr's at the end of their lists */
              source_itr = &source->rules;
              file_itr = &self->file->rules;
            leave_loop:
              /* copy all rules from source_start to source_itr before file_itr */

              LLIST_FORRANGE (source_start, source_itr, n)
                {
                  struct rxpd_rule* rule = rxpd_rule_copy ((struct rxpd_rule*)n);
                  if (!rule)
                    rxpd_die ("rule copy failed\n");

                  llist_insert_prev (file_itr, &rule->node);
                }

              /*iterate for mismatch*/
              file_start = llist_next (file_itr);
              source_start = llist_next (source_itr);
            }

          pth_rwlock_release (&source->lock);
        }
      else
        rxpd_buffer_printf (&self->out, "#ERROR: unknown file '%s'\n", line);
    }

  pth_rwlock_release (&self->file->lock);
  pth_mutex_release (&base->nest_lock);

  rxpd_buffer_printf (&self->out, "#OK:\n");
}

void
rxpd_connection_cmd_FILTER (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;

  /* Filter processes the target list by checking its rules against other lists */
  /* The outcome of this check is used to decide how to process with a rule */
  /* DELETE: delete rule from list */
  /* ACTIVATE: try to reactivate a rule which was commented out */
  /* any other: comment the rule out by using 'any other' as hint*/

  struct rxpd_base* base = self->socket->base;

  pth_mutex_acquire (&base->nest_lock, 0, NULL);
  pth_rwlock_acquire (&self->file->lock, PTH_RWLOCK_RW, FALSE, NULL);

  char* line;
  while ((line = rxpd_buffer_readline (&self->in)))
    {
      if (*line && !RXPD_PREFIXCMP (line, "!EXIT"))
        {

          /* for each line which is a listname */
          struct rxpd_file* source;
          source = (struct rxpd_file*) psplay_find (&base->files, line);
          if (source)
            {
              pth_rwlock_acquire (&source->lock, PTH_RWLOCK_RD, FALSE, NULL);

              LLIST_FOREACH (&self->file->rules, n)
                {
                  LLIST_FOREACH (&source->rules, m)
                    {
                      struct rxpd_rule* rule_n = (struct rxpd_rule*)n;
                      struct rxpd_rule* rule_m = (struct rxpd_rule*)m;
                      if (rule_m->string[0] != '#')
                        {
                          if (regexec (&rule_m->rx, rule_n->string, 0, NULL, 0) == 0)
                            {
                              if (rule_m->atime != (time_t) -1)
                                time (&rule_m->atime);

                              if (RXPD_PREFIXCMP (rule_m->string, "DELETE:"))
                                {
                                  n = llist_prev (n);
                                  rxpd_log (base, LOG_DEBUG, "deleting rule '%s' from list '%s'\n",
                                            rule_n->string, (const char*) self->file->node.key);
                                  rxpd_rule_delete (rule_n);
                                }
                              else if (RXPD_PREFIXCMP (rule_m->string, "ACTIVATE:"))
                                {
                                  if (!rxpd_rule_activate (rule_n))
                                    rxpd_buffer_printf (&self->out, "#ERROR: cant activate rule '%s'\n", rule_n->string);
                                  else
                                    rxpd_log (base, LOG_DEBUG, "activated rule '%s' in list '%s'\n",
                                              rule_n->string, (const char*) self->file->node.key);
                                }
                              else
                                {
                                  char* c = strndupa (rule_m->string, strchr (rule_m->string, ':') - rule_m->string);
                                  rxpd_log (base, LOG_DEBUG, "commenting rule '%s' as '%s', in list '%s'\n",
                                            rule_n->string, c, (const char*) self->file->node.key);

                                  rxpd_rule_comment (rule_n, c);
                                }
                            }
                        }
                    }
                }
              pth_rwlock_release (&source->lock);
            }
          else
            rxpd_buffer_printf (&self->out, "#ERROR: unknown file '%s'\n", line);
        }
      else
        break;  /* exit at empty line, error or whatever */
    }

  pth_rwlock_release (&self->file->lock);
  pth_mutex_release (&base->nest_lock);

  rxpd_buffer_printf (&self->out, "#OK:\n");
}

/* Template
void
rxpd_connection_cmd_ (struct rxpd_connection* self)
{
  RXPD_FILENAME_REQUIRED;
  //struct rxpd_base* base = self->socket->base;
  rxpd_buffer_printf (&self->out, "#ERROR: Unimplemented\n");
}
*/
