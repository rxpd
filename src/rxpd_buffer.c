/*
    rxpd_buffer.c - regex policy daemon

  Copyright (C)
    2007,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rxpd.h"

struct rxpd_buffer*
rxpd_buffer_init (struct rxpd_buffer* self, int fd)
{
  self->fd = fd;
  self->state = RXPD_OK;
  self->eol = self->eob = self->buffer;
  self->buffer [4095] = '\0';
  return self;
}


char*
rxpd_buffer_readline (struct rxpd_buffer* self)
{
  if (self->eol < self->eob)
    {
      //there was a line pending, shift buffer left
      memmove (self->buffer, self->eol+1, self->eob - self->eol - 1);
      self->eob = (char*)(self->eob - (self->eol - self->buffer + 1));
      self->eol = self->buffer;
    }

  do {
    // find next newline, terminate string there
    for (char* i = self->buffer; i < self->eob; ++i)
      {
        if (*i == '\n' || *i == '\r')
          {
            self->eol = i;
            if ((i[1] == '\n' || i[1] == '\r') && i[0] != i[1])
              ++self->eol;

            *i = '\0';
            // have line, return it
            return (self->eob == self->buffer) ? NULL : self->buffer;
          }
      }

    // else we have to read
    if (self->state == RXPD_OK)
      {
        ssize_t r = 0;
        do
          {
            r = pth_read (self->fd, self->eob, 4095 - (self->eob - self->buffer));
          }
        while (r == -1 && errno == EINTR);

        if (r != -1)
          {
            if (r == 0)
              {
                shutdown (self->fd, SHUT_RD);
                self->state = RXPD_EOF;
              }
            self->eob += r;
          }
        else
          self->state = RXPD_ERROR;
      }
  } while (self->state == RXPD_OK);  // TODO while (!buffer overfulls)
  return NULL;
}


int
rxpd_buffer_printf (struct rxpd_buffer* self, const char* fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  int n = vsnprintf (self->buffer, 4096, fmt, ap);
  va_end(ap);

  pth_write (self->fd, self->buffer, n);

  if (n > 4095)
    return 0;

  return 1;
}



